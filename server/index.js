const restify   = require('restify');
const mongoose  = require('mongoose');
const config    = require('./config');
const rjwt      = require('restify-jwt-community');

const server    = restify.createServer();


// Setup Middleware
server.use(restify.plugins.bodyParser());

// Setup Routes
server.use(rjwt({ secret: config.JWT_SECRET }).unless({ path: ['/api','/api/auth', '/api/auth/register'] }));

// Initialise Listening
server.listen(config.PORT, () => {
    mongoose.set('useFindAndModify', false);
    mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true });
});


// Initialise DB Connection
const db = mongoose.connection;

db.on('error', (err) => console.log(err));

db.once('open', () => {
    require('./routes/root')(server);
    require('./routes/customers')(server);
    require('./routes/users')(server);
    console.log(`Server started on port ${config.PORT}`);
});
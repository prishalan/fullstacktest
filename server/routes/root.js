const errors    = require('restify-errors');
const package   = require('../package.json');

module.exports = server => {

    server.get('/api', (req, res, next) => {
        res.send(`API Version ${package.version}`);
        next();
    });
};
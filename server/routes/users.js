const config        = require('../config');
const errors        = require('restify-errors');
const bcrypt        = require('bcryptjs');
const User          = require('../models/User');
const authService   = require('../services/auth-service');
const jwt           = require('jsonwebtoken');


module.exports = server => {

    server.post('/api/auth/register', async (req, res, next) => {
        const { email, password, firstname, lastname } = req.body;

        const user = new User({
            email,
            password,
            firstname,
            lastname
        });

        User.find({ email: user.email }, function (error, docs) {
            if (docs.length){
                return next(new errors.BadRequestError('A user with that email address already exists.'));
            }else{
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(user.password, salt, async (err, hash) => {
                        // hash the password
                        user.password = hash;
                        try {
                            const newUser = await user.save();
                            res.send(201);
                            next();
                        }
                        catch (error) {
                            return next(new errors.InternalError(error.message));
                        }
                    });
                });
            }
        });

    });

    server.post('/api/auth', async (req, res, next) => {
        const { email, password } = req.body;

        try {
            const user = await authService.login(email, password);
            const payload = {
                uid: user._id,
                email: user.email, 
                firstname: user.firstname, 
                lastname: user.lastname
            };
            const token = jwt.sign(payload, config.JWT_SECRET, {
                expiresIn: '20m'
            });
            const { iat, exp } = jwt.decode(token);
            res.send({ iat, exp, token });
            next();
        }
        catch (error) {
            // User is unauthorised
            return next(new errors.UnauthorizedError(error));
        }
    });

};
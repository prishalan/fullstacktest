const bcrypt    = require('bcryptjs');
const mongoose  = require('mongoose');
const User      = mongoose.model('User');

exports.login = (email, password) => {
    return new Promise(async (resolve, reject) => {
        try {
            const user = await User.findOne({ email: email });

            bcrypt.compare(password, user.password, (error, isMatch) => {
                if (error) {
                    // generic error
                    throw error;
                }
                if (isMatch) {
                    resolve(user);
                }
                else {
                    // Password does not match
                    reject('Authentication failed (p)');
                }
            });
        }
        catch (error) {
            // User not found
            reject('Authentication failed (u)');
        }
    });
};

exports.checkUserExists = (email) => {
    return new Promise(async (resolve, reject) => {
        try {
            const user = await User.find({ email: email });

        }
        catch (error) {

        }
    });
};